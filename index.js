//INCLUDE EXPRESS LIBRARY
const express = require('express');
const app = express();
const port = 3000;

// Instantiating the server
app.listen(port, () => {
    console.log('Server ready, listening on http://localhost:' + port);
});

// FUNCTIONS
const getPressureEvent = (req, res) => {
    console.log('Received request for pressure event');
    // Let's retrieve the last event
    var event = getLastEvent();
    // Send back the last event
    res.send(event);
}

const getLastEvent = () => {
  // We create fake random data, when xml will be completed we will take the last value in the xml file
  var eventId = random(1, 10000);
  var shirtsArray = ["model-apparel","boxeur","beach","soccer","style","black","dmc","gonzo"];
  var shirt = shirtsArray[random(1, shirtsArray.length-1)];
  var pressure = random(1, 100);
  var fakeValueObject = {"id": eventId, "shirt":shirt, "pressure":pressure};

  return fakeValueObject
}

const random = (low, high) => {
    return Math.floor(Math.random() * (high - low) + low);
}


// SERVER GET METHODS
// to get this result, call http://localhost:3000/getPressureEvent
app.get('/getPressureEvent', getPressureEvent);

// SERVER POST METHODS
//app.post('/setCustomerEvent', setCustomerEvent);
